#' plunify.
#'
#' @name plunify
#' @docType package
#' @import FSelector scales
#' @importFrom dplyr select filter rename count mutate transmute arrange group_by funs summarise slice matches one_of everything
#' @importFrom magrittr %>% %$% %<>% %T>% set_names divide_by raise_to_power add extract set_colnames set_rownames
#' @importFrom foreach foreach %do% %dopar%
#' @importFrom ggplot2 theme ggplot element_text aes ggplot
#' @importFrom caret train trainControl twoClassSummary createDataPartition createFolds createResample nearZeroVar
#' @importFrom caretEnsemble caretList caretStack caretEnsemble
#' @importFrom logging addHandler logReset writeToConsole writeToFile loginfo removeHandler logdebug logerror
#' @importFrom stringr str_extract str_split
#' @importFrom ROCR prediction performance
#' @importFrom cluster daisy
NULL
