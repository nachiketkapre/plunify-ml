#' Automate data cleaning for csv files
#'
#' @param file_paths File paths for csv files
#' @param ... Optional parameters passed to \code{\link{clean_data}}
#'
#' @return invisible null
#' @export
#'
#' @examples
#' nil
clean_data_auto <- function(file_paths = get_files("data"), ...) {
  attach_handler("clean")
  foreach(file_path = file_paths) %do% {
    read_file(file_path) %>%
      clean_data(...) %>%
      save_csv("clean", extr(file_path, "id") , "cleaned")
  }
  removeHandler("clean")
}

#' Clean data frame
#'
#' @param df Data.frame
#' @param response_regx Regex for response column, refer \code{\link[dplyr]{select}}
#' @param features_regx Regex for feature column
#' @param ... Extra options passed to \code{\link{subset_cols}}
#' @param drop_regx Regex to be dropped
#'
#' @return Cleaned data.frame
#' @export
#'
#' @examples
#' clean_data(df)
clean_data <- function(df, response_regx = "^TNS$", features_regx = "name.*step", drop_regx = "SEED|MULTIPLIER", ...){
  df %>%
    subset_cols(response_regx, features_regx, drop_regx, ...) %>%
    lower_case %>%
    dplyr::select(dplyr::everything(), -log_names(missing_frac(.), names(.), "- missing values"), response) %>%
    dplyr::filter(!log_names(!complete.cases(.), rownames(.), "- incomplete rows")) %>%
    dplyr::select(dplyr::everything(), -log_names(nearZeroVar(.), names(.), "- low variances")) %T>% {
      if(ncol(.) <= 1)
        warning("no feature left")
      if(nrow(.) <= 1)
        warning("no sample left")
    }
}

#' Filter out non-relavant column in a data.frame
#'
#' @param df Data.frame
#' @param res Response regex
#' @param attr Feature regex
#' @param drop Regex of column to be dropped
#' @param ignore.case Logical value to ignore case
#'
#' @return Data.frame with relavant columns left, names of column are modified accordingly based on option ignore.case and ignore.character
#' @export
#'
#' @examples
#' subset_cols(df, "TNS", "\{", "SEED", TRUE)
subset_cols <- function(data, res, attr, drop, ignore.case = TRUE) {
  # data  %>%
    # set_names(names(.) %>% norm_names(ignore.case)) %>%
    # dedup_col(ignore.case) %>%
    extract(data, unique(names(data))) %>%
    select(
      log_names(matches(attr, ignore.case), names(.), "+ features"),
      response = log_names(matches(res, ignore.case), names(.), "+ response")
    ) %>%
    select(everything(), -log_names(matches(drop, ignore.case), names(.), "- dropped features"))
}

log_names <- function(indices, names, msg) {
  loginfo("%s:\n  %s", msg, paste0(names[indices], collapse = if (mean(nchar(names)) < 10) ", " else "\n  "))
  indices
}

#' Strip symbols from character vector and change letter case
#'
#' @param name Character vector
#' @param ignore.case Logical value indicating whether to ignore case
#' @param ignore.character Characters to be stripped
#'
#' @return Character vector
#' @export
#'
#' @examples
#' norm_names("hello {} world", TRUE)
norm_names <- function(names, ignore.case = TRUE) {
  if (ignore.case)
    names %<>% tolower
  gsub("[^a-zA-Z0-9.]+", ".", names) %>%
    gsub("\\.+", ".", .) %>%
    gsub("^\\.+|\\.$", "", .) %>%
    gsub("^([0-9])", "x.\\1", .)
}

#' Generate character translation functions for recursive objects
#'
#'
#' @param fun function, character translation functions like \code{\link{toupper}}, \code{\link{tolower}}
#'
#' @return function equivalent to \code{fun} that applies to recursive objects.
#'
#' @examples
#' unify_case(toupper)

unify_case <- function(fun) {
  function(data) {
    if(is.atomic(data)) {
      if (is.character(data))
        data <- fun(data)
      else if (is.factor(data))
        levels(data) <- unify_case(fun)(levels(data))
    } else {
      data[] <- lapply(data, unify_case(fun))
    }
    data
  }
}

#' Translate characters in recursive objects
#'
#' @param data vector, list, data.frame or matrix that contains character or factor to be translated
#'
#' @return translated object
#' @examples
#' upper_case(iris)
#' lower_case(iris)
#' @name char_trans
NULL

#' @rdname char_trans
#' @export
upper_case <- unify_case(toupper)

#' @rdname char_trans
#' @export
lower_case <- unify_case(tolower)

#' Find column with missing value more than a percentage threshold
#'
#' @param df Data.frame
#' @param frac Numeric, column with more than this fraction of missing value would be returned
#'
#' @return Integer indexes of found columns
#' @export
#'
#' @examples
#' missing_frac(df, 0.3)
missing_frac <- function(data, frac = 0.2, nastrings = c(NA, "", NULL)) {
  if (nrow(data) == 0 || ncol(data) == 0) {
    logging::logwarn("%s: empty data frame, %d cols, %d rows", as.character(match.call()[[1]]), ncol(data), nrow(data))
    seq_along(data)
  } else {
    sapply(data, function(col) mean(col %in% nastrings) > frac) %>% which
  }
}

#' Drop duplicate column in a data.frame
#' @description Check column names only, not values
#'
#' @param df Data.frame
#' @param ignore.case Logical, if True, letter case difference is ignored
#'
#' @return Data.frame with unique columns
#' @export
#'
#' @examples
#' dedup_col(df)
dedup_col <- function(df, ...) {
  # df[, unique_col(names(df), ...)]
  df[, unique(names(df))]
}

#' Get indexes of unique columns in a data.frame
#' @details First index among duplicated columns is returned
#' @param col_names Character vector, column names of data.frame
#' @param ignore.case Logical
#'
#' @return Integer vector, indexes of unique columns
#' @export
#'
#' @examples
#' unique_col(names)
unique_col <- function(col_names, ...) {
  # col_names %<>% norm_names(...)
  tapply(seq_along(col_names), col_names, head, 1) %T>%
  {log_names(-., col_names, "duplicated feature")}
}


