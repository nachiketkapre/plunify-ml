#' Replace value in vector/factor with new value
#'
#' Value would be kept as is if there exists no mappings
#'
#' @param data character vector or factor, data to be processed
#' @param mappings list/data.frame of 2 vectors, first vector is the values to be replaced, second vector is new values
#'
#' @return character/factor with replaced values
#' @export
#'
#' @examples
#' map_names(letters[1:10], mappings = (list(letters[1:5], LETTERS[1:5])))
map_names <- function(data, mappings = get_files("temp", "mapping") %>% read_file) {
  if(is.factor(data)) {
    levels(data) <- map_names(levels(data), mappings)
    return(data)
  }
  mappings %<>% lapply(as.character)
  absent_names <- data %>% .subset(!(. %in% mappings[[1]]))
  mappings %<>% lapply(c, absent_names)
  mappings[[2]][match(data, mappings[[1]])]
}

#' Replace id of character with mappings
#'
#' @param data character/factor to be modified
#' @param ... parameters passed to \code{\link{map_names}}
#'
#' @return modified character/factor
#' @export
#'
#' @examples
#' modify_names(paste0(letters[1:10], "_extra_string"), mappings = (list(letters[1:5], LETTERS[1:5])))
modify_ids <- function(data, ...) {
  old_ids <- extr(data, "id")
  new_ids <- map_names(old_ids, ...)
  foreach(file = data, id = old_ids, new = new_ids, .combine = c) %do% sub(id, new, file)
}

#' Rename files with id mappings
#'
#' @param files character vector of file names
#' @param ... parameters passed to \code{\link{map_names}}
#'
#' @return modified file names
#' @export
#'
#' @examples
#' nil
rename_files <- function(files, ...) {
  file.rename(files, modify_names(files, ...))
}

#' Modify specs in metric csv files according to id mappings
#'
#' @param files character vector of file names
#' @param mappings id mappings passed to \code{\link{map_names}}
#'
#' @return null
#' @export
#'
#' @examples
#' nil
rename_specs <- function(files, mappings) {
  foreach(file = files) %dopar% {
    read_file(file) %>%
      mutate(specs = modify_ids(specs, mappings)) %>%
      write.csv(file = file, row.names = FALSE)
  }
}
