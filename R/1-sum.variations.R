get_best_parameters <- function(files = get_files("clean", "cleaned", "/generic")){
  best_configs <- foreach(file = files) %do% {
    read_file(file) %>%
      dplyr::arrange(desc(response)) %>%
      dplyr::slice(1)
  }
  combine_data(best_configs) %>%
    dplyr::select(-response) %>%
    set_rownames(extr(files, "id"))
}

default_theme <- function() {
  theme(
    axis.line=element_line(color='black'),
    legend.position="right",
    legend.background=element_blank(),
    # legend.title=element_blank(),
    axis.title = element_blank(),
    legend.key=element_blank(),
    legend.key.width=unit(0.3,"cm"),
    legend.key.height=unit(0.35,"cm"),
    legend.text=element_text(size=8),
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1)
  )
}

plot_dissim <- function(best_par) {
  best_par[] <- lapply(best_par, . %>% {if(is.character(.)) as.factor(.) else .})
  dfs <- cluster::daisy(best_par) %>% as.matrix
  dfs[upper.tri(dfs)] <- NA
  ggplot(
    data = subset(reshape2::melt(dfs), !is.na(value)),
    aes(x=Var1, y=Var2, fill = value)
  ) +
    geom_tile() +
    scale_fill_gradient(low = "white", high ="black", space = "Lab", name = NULL)
}
