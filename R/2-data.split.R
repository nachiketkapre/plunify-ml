#' Partition data.frame
#'
#' @param df Data.frame to be splitted
#' @param ... Named parameters e.g. \code{training = 0.5}, name is the name of partition, value is the fraction of samples to include in the partition.
#'
#' @return List of partitioned data.frames
#' @export
#'
#' @examples
#' partition_df(df, training = 0.6, testing = 0.4)
partition_df <- function(df, ...){
  if (!is.data.frame(df))
    stop("error, not data frame")
  options <- c(...)
  if (length(options) == 0)
    return()
  else {
    if(options[1] > 1)
      stop("fractions sum exceeds 1")
    set.seed(42)
    fs_index <- createDataPartition(df$response, p = options[1], list = FALSE)
    c(
      list(df[fs_index, ]),
      do.call(partition_df, c(list(df = df[-fs_index, ]), options[-1] / (1 - options[1])))
    ) %>%
      set_names(names(options))
  }
}


#' Automate csv file partitioning
#'
#' @param files csv files
#'
#' @return nil
#' @export
#'
#' @examples
#' nil
partition_df_auto <- function(files = get_files("clean", pattern = "cleaned")){
  attach_handler("partition")
  files %>% lapply(function(file_path){
    df <- read_file(file_path)
    if(nrow(df) >= 60) {
      partition_df(df, feature = 1/3, training = 1/2, testing = 1/6) %>% {
        foreach(df = ., fname = names(.)) %do%
          save_csv(df, "clean", extr(file_path, "id"), fname)
      }
    }
  })
  removeHandler("partition")
}


#' Ensure compatibility between training set and testing set
#'
#' @param train_set Data.frame, training set
#' @param test_set Data.frame, testing set
#' @details Actions to be performed
#' \enumerate{
#'  \item mark factor values in testing set but not in training set as NA
#'  \item remove factors with only 1 level
#' }
#'
#' @return List of data.frame, train_set and test_set
#' @export
#'
#' @examples
#' \dontrun{
#'  update_data(train_set, testset)
#' }
update_data <- function(train_set, test_set) {
  train_set %<>% droplevels
  for (i in 1:ncol(test_set)) {
    if (is.factor(test_set[[i]]))
      test_set[[i]] <- factor(test_set[[i]], levels = levels(train_set[[i]]))
  }
  non_single <- which(sapply(train_set, function(x) length(unique(x)) >= 2))
  list(train_set = train_set[, non_single], test_set = test_set[, non_single])
}

# TODO
not_near_zero_var <- function(train_set, ...) {
  index <- caret::nearZeroVar(train_set)
  setdiff(seq_along(df), index)
}
