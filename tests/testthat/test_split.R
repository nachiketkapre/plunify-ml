test_that("data partitioning", {
  df <- data.frame(replicate(10, sample(1:1010, 60)), response = sample(c("YES","no"), 60, TRUE))
  dfs <- partition_df(df)
  # expect_equal(nrow(dfs$feature), 20)
  # expect_equal(nrow(dfs$train), 30)
  # expect_equal(nrow(dfs$test), 10)
})

test_that("new levels in testing set are coerced to NA", {
  df <- iris
  df2 <- dplyr::mutate(iris, Species = factor(c("not a flower", Species[-1])))
  updated <- update_data(df, df2)
  expect_is(updated, "list")
  expect_false(is.na(updated$train_set$Species[1]))
  expect_true(is.na(updated$test_set$Species[1]))
})
