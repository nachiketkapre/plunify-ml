# R library for Plunify's Machine Learning routines
### *Yanghua Que*, Nachiket Kapre, Harnhua Ng
### Please cite: "Boosting Convergence of Timing Closure using Feature Selection in a Learning-Driven Approach", FPL 2016. http://nachiket.github.io/publications/intime_fpl2016.pdf 


### Installation and setup R dependencies and library
```
#!R

install.packages("devtools",repos="http://cran.case.edu")
devtools::install_bitbucket("spinosae/plunify-ml")

```

### 0. Load a csv file/data -- here built-in dataset 'BeMicro' is used
```
#!R

data("BeMicro")
BeMicro[] <- lapply(BeMicro, function(col) {if(is.character(col)) as.factor(col) else col})
cleaned <- plunify::clean_data(BeMicro); 
cleaned$response <- plunify::factorize_v(cleaned$response); 
parts <- plunify::partition_df(cleaned, feature =  1/3, training = 1/2, testing = 1/6); 

```


### 1. Feature selection
```
#!R

feature_ranking <-  plunify::select_feature(parts$feature)$information.gain
feature_subset <- plunify::slice_df(parts$training, parts$testing, feature_ranking, sample_no = nrow(parts$training), feature_no = 8)

```

### 2. Run classifiers to train
```
#!R

models <- plunify::train_ensemble(feature_subset$train_set, plunify::get_options("ensem_list")[1:3])

```

### 3. Test predictions against known results
```
#!R

predictions <- plunify::predict_ensemble(models, dplyr::select(feature_subset$test_set, -response))
performance <- plunify::eval_ensemble(predictions, feature_subset$test_set$response)
dir.create("temp")
plunify::save_perfs(performance, "temp", "BeMicro", "algo");

```