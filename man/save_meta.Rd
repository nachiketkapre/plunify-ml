% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/1-sum.meta.R
\name{save_meta}
\alias{save_meta}
\title{Print and save meta table}
\usage{
save_meta(meta, file = "report/bench_combined.tex")
}
\arguments{
\item{meta}{data.frame, meta info}
}
\value{
null
}
\description{
Print and save meta table
}
\examples{
nil
}

